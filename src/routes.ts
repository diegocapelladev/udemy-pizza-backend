import { Router } from 'express'
import multer from 'multer'

import { CreateUserController } from './controllers/user/CreateUserController'
import { AuthUserController } from './controllers/user/AuthUserController'
import { DetailUserController } from './controllers/user/DetailUserController'

import { isAuthenticated } from './middlewares/isAuthenticated'

import { CreateCategoryController } from './controllers/category/CreateCategoryController'
import { ListCategoryController } from './controllers/category/ListCategoryController'

import { CreateProductController } from './controllers/product/CreateProductController'
import { ListByCategoryController } from './controllers/product/ListByCategoryController'

import { CreateOrderController } from './controllers/order/CreateOrderController'
import { RemoveOrderController } from './controllers/order/RemoveOrderController'
import { AddItemController } from './controllers/order/AddItemController'
import { RemoveItemController } from './controllers/order/RemoveItemController'
import { SendOrderController } from './controllers/order/SendOrderController'
import { ListOrderController } from './controllers/order/ListOrderController'
import { DetailOrderController } from './controllers/order/DetailOrderController'
import { FinishOrderController } from './controllers/order/FinishOrderController'

import uploadCOnfig from './config/multer'

const router = Router()

const upload = multer(uploadCOnfig.upload('./tmp'))

const createUserController = new CreateUserController()
const authUserController = new AuthUserController()
const detailUserController = new DetailUserController()
const createCategoryController = new CreateCategoryController()
const listCategoryController = new ListCategoryController()
const createProductController = new CreateProductController()
const listByCategoryController = new ListByCategoryController()
const createOrderController = new CreateOrderController()
const removeOrderController = new RemoveOrderController()
const addItemController = new AddItemController()
const removeItemController = new RemoveItemController()
const sendOrderController = new SendOrderController()
const listOrderController = new ListOrderController()
const detailOrderController = new DetailOrderController()
const finishOrderController = new FinishOrderController()

// Users
router.post('/users', createUserController.handle)

router.post('/session', authUserController.handle)

router.get('/me', isAuthenticated, detailUserController.handle)

// Category
router.post('/category', isAuthenticated, createCategoryController.handle)

router.get('/category', isAuthenticated, listCategoryController.handle)

// Product
router.post(
  '/product',
  isAuthenticated,
  upload.single('file'),
  createProductController.handle
)

router.get(
  '/product/category',
  isAuthenticated,
  listByCategoryController.handle
)

// Order
router.post('/order', isAuthenticated, createOrderController.handle)

router.delete('/order/:order_id', isAuthenticated, removeOrderController.handle)

router.post('/item', isAuthenticated, addItemController.handle)

router.delete('/item/:item_id', isAuthenticated, removeItemController.handle)

router.put('/order/draft', isAuthenticated, sendOrderController.handle)

router.get('/order/list', isAuthenticated, listOrderController.handle)

router.get('/order/:order_id', isAuthenticated, detailOrderController.handle)

router.put('/order', isAuthenticated, finishOrderController.handle)

export { router }
