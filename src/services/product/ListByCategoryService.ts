import prismaClient from '../../prisma'

interface ProductCategoryRequest {
  category_id: string
}

class ListByCategoryService {
  async execute({ category_id }: ProductCategoryRequest) {
    const findByCategory = await prismaClient.product.findMany({
      where: {
        category_id
      }
    })

    return findByCategory
  }
}

export { ListByCategoryService }
