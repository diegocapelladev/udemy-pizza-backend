import prismaClient from '../../prisma'

class ListOrderService {
  async execute() {
    const listOrder = prismaClient.order.findMany({
      where: {
        status: false,
        draft: false
      },
      orderBy: {
        created_at: 'desc'
      }
    })

    return listOrder
  }
}

export { ListOrderService }
