import prismaClient from '../../prisma'

interface UpdateStatusOrder {
  order_id: string
}

class FinishOrderService {
  async execute({ order_id }: UpdateStatusOrder) {
    const updateStatus = await prismaClient.order.update({
      where: {
        id: order_id
      },
      data: {
        status: true
      }
    })

    return updateStatus
  }
}

export { FinishOrderService }
