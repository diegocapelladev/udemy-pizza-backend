import prismaClient from '../../prisma'

interface OderDetailRequest {
  order_id: string
}

class DetailOrderService {
  async execute({ order_id }: OderDetailRequest) {
    const orderDetail = prismaClient.item.findMany({
      where: {
        order_id: order_id
      },
      include: {
        product: true,
        order: true
      }
    })

    return orderDetail
  }
}

export { DetailOrderService }
