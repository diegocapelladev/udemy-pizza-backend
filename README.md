# Pizza Back-end API

## Getting Started

### Set ENV
PORT=3333

DATABASE_URL="postgresql://<user>:<password>@localhost:5432/<database>?schema=public"

JWT_SECRET=""

### RUN

```bash
npm run dev
# or
yarn dev
```

Open [http://localhost:3333](http://localhost:3333) with your browser to see the result.

## Pizza Project Front-end

The FRONT-END can be accessed on [https://gitlab.com/diegocapelladev/udemy-pizza-frontend](https://gitlab.com/diegocapelladev/udemy-pizza-frontend)


## Pizza Project Mobile

The mobile app can be accessed on [https://gitlab.com/diegocapelladev/udemy-pizza-mobile](https://gitlab.com/diegocapelladev/udemy-pizza-mobile)
